package mk.plugin.ncvp.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

import mk.plugin.ncvp.lang.Lang;
import mk.plugin.ncvp.utils.GUIUtils;

public class PlayerCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (cmd.getName().equalsIgnoreCase("cuonghoa")) {
				if (!player.hasPermission("nangcapvatpham.open.cuonghoa")) {
					player.sendMessage(Lang.NO_PERMISSION.get());
					return false;
				}
				GUIUtils.openGUI(player, "gui-cuonghoa", Maps.newHashMap());
			}
			else if (cmd.getName().equalsIgnoreCase("dunghop")) {
				if (!player.hasPermission("nangcapvatpham.open.dunghop")) {
					player.sendMessage(Lang.NO_PERMISSION.get());
					return false;
				}
				GUIUtils.openGUI(player, "gui-dunghop", Maps.newHashMap());
			}
		}
		else sender.sendMessage("§cVào game mà xài lệnh");

		
		return false;
	}
	
}
