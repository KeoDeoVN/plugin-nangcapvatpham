package mk.plugin.ncvp.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import mk.plugin.ncvp.config.Configs;
import mk.plugin.ncvp.gui.nangcap.UpgradeUtils;
import mk.plugin.ncvp.lang.Lang;
import mk.plugin.ncvp.main.MainNCVP;
import mk.plugin.ncvp.utils.GUIUtils;
import mk.plugin.ncvp.utils.Utils;

public class AdminCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		if (!sender.hasPermission("nangcapvatpham.admin")) {
			sender.sendMessage(Lang.NO_PERMISSION.get());
			return false;
		}
		
		try {
			
			if (args[0].equalsIgnoreCase("reload")) {
				MainNCVP.get().reloadConfigs();
				sender.sendMessage("§aConfiguration reloaded!");
			}
			
			else if (args[0].equalsIgnoreCase("open")) {
				Player player = Bukkit.getPlayer(args[1]);
				String guiID = "gui-" + args[2].replace("gui-", "");
				GUIUtils.openGUI(player, guiID, Maps.newHashMap());
				sender.sendMessage("§aOpened gui " + guiID + " for " + player.getName());
			}
			
			else if (args[0].equalsIgnoreCase("givestone")) {
				Player player = Bukkit.getPlayer(args[1]);
				String stoneID = args[2];
				Utils.giveItem(player, Configs.STONE_ITEMS.get(stoneID).getItemStack());
				sender.sendMessage("§aGave stone " + stoneID + " for " + player.getName());
			}
			
			else if (args[0].equalsIgnoreCase("giveamulet")) {
				Player player = Bukkit.getPlayer(args[1]);
				String amuletID = args[2];
				Utils.giveItem(player, Configs.AMULET_ITEMS.get(amuletID).getItemStack());
				sender.sendMessage("§aGave amulet " + amuletID + " for " + player.getName());
			}
			
			else if (args[0].equalsIgnoreCase("setitem")) {
				Player player = (Player) sender;
				ItemStack is = player.getInventory().getItemInMainHand();
				if (is == null) {
					sender.sendMessage("§cItem null");
					return false;
				}
				UpgradeUtils.setUpgradeable(is);
			}
			
			else if (args[0].equalsIgnoreCase("upgrade")) {
				Player player = (Player) sender;
				ItemStack is = player.getInventory().getItemInMainHand();
				if (!UpgradeUtils.canUpgrade(is)) UpgradeUtils.setUpgradeable(is);
				UpgradeUtils.upgrade(is, Integer.valueOf(args[1]));
				player.updateInventory();
			}
			
		}
		catch (ArrayIndexOutOfBoundsException e) {
			sendHelp(sender);
		}
		
		return false;
	}
	
	public void sendHelp(CommandSender sender) {
		sender.sendMessage("");
		sender.sendMessage("§2§lNangCapVatPham 1.0 by MankaiStep (/nca, /ncvp)");
		sender.sendMessage("§a/nca reload: §7Reload config");
		sender.sendMessage("§a/nca open <player> <gui(cuonghoa|dunghop)>: §7Mở GUI cho người chơi");
		sender.sendMessage("§a/nca givestone <player> <id>: §7Đưa stone cho người chơi");
		sender.sendMessage("§a/nca giveamulet <player> <id>: §7Đưa bùa hộ mệnh cho người chơi");
		sender.sendMessage("§a/nca setitem: §7Khiến item trên tay có thể cường hóa");
		sender.sendMessage("§a/nca upgrade <level>: §7Cường hóa item trên tay đến cấp độ ...");
		sender.sendMessage("");
	}

}
