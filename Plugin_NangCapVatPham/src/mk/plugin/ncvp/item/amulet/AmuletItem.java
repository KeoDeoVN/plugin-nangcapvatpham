package mk.plugin.ncvp.item.amulet;

import org.bukkit.inventory.ItemStack;

import mk.plugin.ncvp.config.Configs;

public class AmuletItem {
	
	private double bonusChance;
	private ItemStack itemStack;
	
	public AmuletItem(double bonusChance, ItemStack itemStack) {
		this.bonusChance = bonusChance;
		this.itemStack = itemStack;
	}
	
	public double getBonusChance() {
		return this.bonusChance;
	}
	
	public ItemStack getItemStack() {
		return this.itemStack;
	}
	
	public static AmuletItem match(ItemStack is) {
		for (AmuletItem ai : Configs.AMULET_ITEMS.values()) {
			if (ai.getItemStack().isSimilar(is)) return ai;
		}
		return null;
	}
	
	public static boolean isAmuletItem(ItemStack is) {
		return match(is) != null;
	}
	
}
