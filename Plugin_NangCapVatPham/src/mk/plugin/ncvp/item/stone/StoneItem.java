package mk.plugin.ncvp.item.stone;

import org.bukkit.inventory.ItemStack;

import mk.plugin.ncvp.config.Configs;

public class StoneItem {
	
	private String id;
	private double multipleChance;
	private ItemStack itemStack;
	
	public StoneItem(String id, double multipleChance, ItemStack itemStack) {
		this.id = id;
		this.multipleChance = multipleChance;
		this.itemStack = itemStack;
	}
	
	public String getID() {
		return this.id;
	}
	
	public double getMultipleChance() {
		return this.multipleChance;
	}
	
	public ItemStack getItemStack() {
		return this.itemStack.clone();
	}
	
	public static StoneItem match(ItemStack is) {
		for (StoneItem ai : Configs.STONE_ITEMS.values()) {
			if (ai.getItemStack().isSimilar(is)) return ai;
		}
		return null;
	}
	
	public static boolean isStoneItem(ItemStack is) {
		return match(is) != null;
	}
	
}
