package mk.plugin.ncvp.listener;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import mk.plugin.ncvp.gui.GUI;
import mk.plugin.ncvp.gui.GUIHolder;
import mk.plugin.ncvp.gui.Icon;
import mk.plugin.ncvp.gui.nangcap.UpgradeUtils;
import mk.plugin.ncvp.lang.Lang;
import mk.plugin.ncvp.main.MainNCVP;
import mk.plugin.ncvp.utils.GUIUtils;

public class GUIListener implements Listener {
	
	/*
	 * Anvil cancel
	 */
	
	@EventHandler
	public void onAnvil2(InventoryClickEvent e) {
		ItemStack is = e.getCurrentItem();
		if (UpgradeUtils.canUpgrade(is)) {
			e.setCancelled(true);
			return;
		}
	}
	
	/*
	 * On GUI Closed
	 */
	@EventHandler
	public void onGUIClose(InventoryCloseEvent e) {
		Inventory inv = e.getInventory();
		if (inv == null) return;
		Player player = (Player) e.getPlayer();
		GUIUtils.checkCheck(player, MainNCVP.get());
		GUIUtils.dropItems(player, inv);
	}
	
	/*
	 * On GUI Top Clicked
	 */
	@EventHandler
	public void onTopClick(InventoryClickEvent e) {
		Inventory inv = e.getClickedInventory();
		if (inv == null) return;
		GUI gui = GUIUtils.fromInventory(inv);
		if (gui == null) return;
		int slot = e.getSlot();
		Player player = (Player) e.getWhoClicked();
		if (player.getOpenInventory().getTopInventory() != inv) return;
		e.setCancelled(true);
		
		// Get
		ItemStack cursor = e.getCursor();
		if ((cursor == null || cursor.getType() == Material.AIR)) {
			if (e.getClick() == ClickType.LEFT) {
				gui.getHandler().resetButton(inv, player);
			}
		}
		// Cant place
		else {
			player.sendMessage(Lang.GUI_KHONGTHEDAT.get());
			return;
		}
		
		// Top click
		gui.getHandler().onTopClick(inv, player, slot, e.getClick(), e);
		
		// Button
		Icon icon = gui.getIcon(slot);
		if (icon == null) return;
		if (icon.getSlotType().equalsIgnoreCase("button") && e.getClick() != ClickType.SHIFT_LEFT && e.getClick() != ClickType.SHIFT_RIGHT) {
			gui.getHandler().onButtonClick(inv, player, e);
		}
		
		// GUI chance
		if (icon.getSlotType().equalsIgnoreCase("dunghop")) {
			player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
			GUIUtils.openGUI(player, "gui-dunghop", Maps.newHashMap());
		}
		else if (icon.getSlotType().equalsIgnoreCase("cuonghoa")) {
			player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
			GUIUtils.openGUI(player, "gui-cuonghoa", Maps.newHashMap());
		}
	}
	
	/*
	 * On Bot Click
	 */
	@EventHandler
	public void onBotClick(InventoryClickEvent e) {
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (inv == null) return;
		GUI gui = GUIUtils.fromInventory(inv);
		if (gui == null) return;
		e.setCancelled(true);
		
		int slot = e.getSlot();
		Player player = (Player) e.getWhoClicked();
		if (player.getOpenInventory().getBottomInventory() != e.getClickedInventory()) return;
		Inventory botinv = player.getOpenInventory().getBottomInventory();
		
		// Bot click
		gui.getHandler().onBotClick(player.getOpenInventory().getTopInventory(), botinv, player, slot, e.getClick(), e);
	}
	
	/*
	 * Cancel drag
	 */
	@EventHandler
	public void onInvDrage(InventoryDragEvent e) {
		Inventory inv = e.getInventory();
		if (inv.getHolder() instanceof GUIHolder) e.setCancelled(true);
	}
	
	
	/*
	 * Cancel anvil
	 */
	@EventHandler
	public void onAnvil(InventoryClickEvent e) {
		if (e.getView().getType() == InventoryType.ANVIL || e.getWhoClicked().getOpenInventory().getTopInventory().getType() == InventoryType.ANVIL) {
			if (e.getClick() == ClickType.NUMBER_KEY) e.setCancelled(true);
			ItemStack clicked = e.getCurrentItem();
			if (UpgradeUtils.canUpgrade(clicked)) e.setCancelled(true);
		}
	}
	
	
}
