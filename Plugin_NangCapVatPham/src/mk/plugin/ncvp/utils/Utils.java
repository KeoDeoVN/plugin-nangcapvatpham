package mk.plugin.ncvp.utils;

import java.text.DecimalFormat;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class Utils {
	
	public static String getEscapeRegex(String s) {
		return s.replaceAll("([\\\\\\.\\[\\{\\(\\*\\+\\?\\^\\$\\|])", "\\\\$1");
	}
	
	public static String getDoubleRegex() {
		return "\\d+(\\.\\d+)?.?";
	}
	
	public static double round(double i) {
		DecimalFormat df = new DecimalFormat("#.##"); 
		String s = df.format(i).replace(",", ".");
		double newDouble = Double.valueOf(s);
		
		return newDouble;
	}
	
	public static String checkPlaceholders(String s, Map<String, String> placeholders) {
		for (Entry<String, String> e : placeholders.entrySet()) {
			s = s.replace("%" + e.getKey() + "%", e.getValue());
		}
		return s;
	}
	
	public static void giveItem(Player player, ItemStack item) {
		PlayerInventory inv = player.getInventory();
		inv.addItem(item);
		
	}
	
	public static boolean rate(double tiLe) {
		if (tiLe >= 100) return true;
		double rate = tiLe * 100;
		int random = new Random().nextInt(10000);
		if (random < rate) {
			return true;
		} else return false;
	}
	
}
