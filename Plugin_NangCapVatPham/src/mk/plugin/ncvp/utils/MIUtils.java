package mk.plugin.ncvp.utils;

import java.util.Map;

import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;
import com.praya.myitems.MyItems;
import com.praya.myitems.manager.game.LoreStatsManager;
import com.praya.myitems.manager.game.SocketManager;

import api.praya.myitems.builder.lorestats.LoreStatsEnum;
import api.praya.myitems.builder.lorestats.LoreStatsOption;
import api.praya.myitems.main.MyItemsAPI;
import api.praya.myitems.manager.game.LoreStatsManagerAPI;
import mk.plugin.ncvp.gui.nangcap.LoreStat;

public class MIUtils {
	
//	public static boolean isMIItem(ItemStack item) {
//		return getStats(item).size() > 1;
//	}
	
	public static SocketManager getSocketManager() {
		return MyItems.getPlugin(MyItems.class).getGameManager().getSocketManager();
	}
	
	public static LoreStatsManagerAPI getLoreStatManagerAPI() {
		return MyItemsAPI.getInstance().getGameManagerAPI().getLoreStatsManagerAPI();
	}
	
	public static LoreStatsManager getLoreStatManager() {
		return MyItems.getPlugin(MyItems.class).getGameManager().getStatsManager();
	}
	
	public static Map<LoreStatsEnum, LoreStat> getStats(ItemStack item) {
		Map<LoreStatsEnum, LoreStat> map = Maps.newHashMap();
		if (!ItemStackUtils.hasLore(item)) return map;
		LoreStatsManagerAPI lsm = getLoreStatManagerAPI();
		for (LoreStatsEnum stat : LoreStatsEnum.values()) {
			double min = lsm.getLoreValue(item, stat, LoreStatsOption.MIN);
			double max = lsm.getLoreValue(item, stat, LoreStatsOption.MAX);			
			if (stat == LoreStatsEnum.CRITICAL_DAMAGE) {
				min++;
				max++;
			};
			if (min == max && min == 0) continue;
			
			// Check if is this line
			map.put(stat, new LoreStat(getLoreStatManager().getLineLoreStats(item, stat), min, max));
		}
		
		return map;
	}
	
	public static String getStatLine(LoreStatsEnum stat, LoreStat ls) {
		return getLoreStatManager().getTextLoreStats(stat, Utils.round(ls.getValue()), Utils.round(ls.getMaxValue()));
	}
	
	
	
	
	
	
	
}
