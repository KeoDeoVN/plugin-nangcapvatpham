package mk.plugin.ncvp.utils;

import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.ncvp.gui.GUI;
import mk.plugin.ncvp.gui.GUIHandler;
import mk.plugin.ncvp.gui.GUIHolder;
import mk.plugin.ncvp.gui.Icon;
import mk.plugin.ncvp.gui.dunghop.FusionGUIHandler;
import mk.plugin.ncvp.gui.nangcap.UpgradeGUIHandler;
import mk.plugin.ncvp.main.MainNCVP;
import mk.plugin.ncvp.yaml.YamlFile;

public class GUIUtils {
	
	public static Map<String, GUI> guis = Maps.newHashMap();
	
	public static void dropItems(Player player, Inventory inv) {
		GUI gui = GUIUtils.fromInventory(inv);
		if (gui == null) return;
		gui.getIcons().forEach((slot, icon) -> {
			if (icon.isCloseDrop()) {
				ItemStack item = GUIUtils.getItem(inv, slot);
				if (item != null) player.getInventory().addItem(item);
			}
		});
	}
	
	public static void loadAll() {;
		load("gui-cuonghoa", YamlFile.GUI_CUONGHOA.get(), new UpgradeGUIHandler());
		load("gui-dunghop", YamlFile.GUI_DUNGHOP.get(), new FusionGUIHandler());
	}
	
	public static void load(String name, FileConfiguration config, GUIHandler handler) {
		String title = config.getString("title").replace("&", "§");
		int rows = config.getInt("row");
		
		Map<Integer, Icon> icons = Maps.newHashMap();
		config.getConfigurationSection("icons").getKeys(false).forEach(id -> {
			List<Integer> slots = Lists.newArrayList();
			if (config.isList("icons." + id + ".slot")) slots = config.getIntegerList("icons." + id + ".slot");
			else slots.add(config.getInt("icons." + id + ".slot"));
			String slotType = config.getString("icons." + id + ".slot-type").toLowerCase();
			ItemStack item = ItemBuilder.buildItem(config.getConfigurationSection("icons." + id + ".item"));
			boolean dropOnClose = config.getBoolean("icons." + id + ".close-drop");
			Icon gi = new Icon(slotType, dropOnClose, item);
			
			// Put
			slots.forEach(slot -> {
				icons.put(slot, gi);
			});

		});
		
		GUI gui = new GUI(title, rows, icons, handler);
		guis.put(name, gui);
	}
	
	public static int toSlot(int x, int y) {
		return (y - 1) * 9 + x - 1;
	}
	
	public static GUI fromInventory(Inventory inv) {
		InventoryHolder holder = inv.getHolder();
		if (holder instanceof GUIHolder == false) return null;
		return guis.get(((GUIHolder) holder).getGUIID());
	}
	
	public static boolean isDefaultIcon(Inventory inv, int slot) {
		GUI gui = fromInventory(inv);
		ItemStack item = inv.getItem(slot);
		if (item == null) return false;
		if (!gui.getIcons().containsKey(slot)) return false;
		if (item.getType() != gui.getIcon(slot).getItem(null).getType()) return false;
		
		return item.isSimilar(gui.getIcon(slot).getDefaultItem());
	}
	
	public static ItemStack getDefaultIcon(Inventory inv, String type) {
		GUI gui = fromInventory(inv);
		for (Icon i : gui.getIcons().values()) {
			if (i.getSlotType().equalsIgnoreCase(type)) return i.getDefaultItem();
		}
		return null;
	}
	
	public static ItemStack getDefaultIcon(Inventory inv, int slot) {
		GUI gui = fromInventory(inv);
		if (!gui.getIcons().containsKey(slot)) return null;
		return gui.getIcon(slot).getItem(null);
	}
	
	public static String getSlotType(Inventory inv, int slot) {
		GUI gui = fromInventory(inv);
		if (!gui.getIcons().containsKey(slot)) return null;
		return gui.getIcon(slot).getSlotType();
	}
	
	public static boolean isInType(Inventory inv, int slot, String type) {
		String typeA = getSlotType(inv, slot);
		return typeA != null && typeA.equalsIgnoreCase(type);
	}
	
	public static List<ItemStack> getItemList(Inventory inv, String type) {
		List<ItemStack> list = Lists.newArrayList();
		GUI gui = fromInventory(inv);
		for (int slot : gui.getIcons().keySet()) {
			if (!isDefaultIcon(inv, slot) && gui.getIcon(slot).getSlotType().equals(type)) 
				list.add(inv.getItem(slot));
		}
		return list;
	}
	
	public static boolean fillAll(Inventory inv, String type) {
		return getItemList(inv, type).size() == count(inv, type);
	}
	
	public static int count(Inventory inv, String type) {
		int c = 0;
		GUI gui = fromInventory(inv);
		for (int slot : gui.getIcons().keySet()) {
			if (gui.getIcon(slot).getSlotType().equals(type)) 
				c++;
		}
		return c;
	}
	
	public static ItemStack getItem(Inventory inv, String type) {
		List<ItemStack> list = getItemList(inv, type);
		if (list.size() == 0) return null;
		return list.get(0);
	}
	
	public static Inventory openGUI(Player player, String name, Map<String, String> placeholders) {
		GUI gui = guis.get(name);
		Inventory inv = Bukkit.createInventory(new GUIHolder(name), gui.getRow() * 9, gui.getTitle());
		player.openInventory(inv);
		Bukkit.getScheduler().runTaskAsynchronously(MainNCVP.get(), () -> {
			gui.getIcons().forEach((slot, icon) -> {
				ItemStack item = icon.getItem(placeholders);
				inv.setItem(slot, item);
			});
			gui.getHandler().resetButton(inv, player);
		});
		return inv;
	}
	
	public static boolean placeItem(Inventory inv, ItemStack item, String type) {
		List<Integer> slots = Lists.newArrayList();
		GUI gui = fromInventory(inv);
		gui.getIcons().forEach((slot, icon) -> {
			if (icon.getSlotType().equalsIgnoreCase(type)) slots.add(slot);
		});
		return placeItem(inv, item, slots);
	}
	
	public static boolean placeItem(Inventory inv, ItemStack item, int slot) {
		return placeItem(inv, item, Lists.newArrayList(slot));
	}
	
	public static boolean placeItem(Inventory inv, ItemStack item, List<Integer> slots) {
		for (int slot : slots) {
			if (!isDefaultIcon(inv, slot)) continue;
			if (item == null || item.getType() == Material.AIR) continue;
			ItemStack clone = item.clone();
			clone.setAmount(1);
			inv.setItem(slot, clone);
			
			int amount = item.getAmount();
			if (amount == 0) item.setType(Material.AIR);
			else item.setAmount(amount - 1);	
			
			return true;
		}
		
		return false;
	}
	
	public static ItemStack getItem(Inventory inv, int slot) {
		GUI gui = fromInventory(inv);
		if (!gui.hasIcon(slot)) return null;
		if (isDefaultIcon(inv, slot)) return null;
		ItemStack item = inv.getItem(slot);
		inv.setItem(slot, gui.getIcon(slot).getItem(null));
		return item;
	}
	
	public static void setItem(Inventory inv, String type, ItemStack item) {
		GUI gui = fromInventory(inv);
		gui.getIcons().forEach((slot, icon) -> {
			if (!icon.getSlotType().equalsIgnoreCase(type)) return;
			inv.setItem(slot, item);
		});
	}
	
	public static void resetItem(Inventory inv, String type, Map<String, String> placeholders) {
		GUI gui = fromInventory(inv);
		gui.getIcons().forEach((slot, icon) -> {
			if (!icon.getSlotType().equalsIgnoreCase(type)) return;
			ItemStack item = icon.getItem(placeholders);
			inv.setItem(slot, item);
		});
	}
	
	public static void addCheck(Player player, Plugin plugin) {
		player.setMetadata("checkGUI", new FixedMetadataValue(plugin, ""));
	}
	
	public static void removeCheckGUI(Player player, Plugin plugin) {
		player.removeMetadata("checkGUI", plugin);
	}
	
	public synchronized static boolean checkCheck(Player player, Plugin plugin) {
		if (player.hasMetadata("checkGUI")) {
			removeCheckGUI(player, plugin);
			return false;
		} 
		addCheck(player, plugin);
		
		Bukkit.getScheduler().runTaskLater(plugin, () -> {
			if (player.hasMetadata("checkGUI")) {
				removeCheckGUI(player, plugin);
			}
		}, 2);
		return true;
	}
	
}
