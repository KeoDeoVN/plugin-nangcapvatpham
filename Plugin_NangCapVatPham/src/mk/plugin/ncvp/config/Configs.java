package mk.plugin.ncvp.config;

import java.util.List;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import api.praya.myitems.builder.lorestats.LoreStatsEnum;
import mk.plugin.ncvp.item.amulet.AmuletItem;
import mk.plugin.ncvp.item.stone.StoneItem;
import mk.plugin.ncvp.utils.ItemBuilder;

public class Configs {
	
	public static int TITLE_SHOW;
	public static int TITLE_FADE;
	public static String LEVEL_LINE_FORMAT;
	public static List<String> ALLOWED_MATERIALS = Lists.newArrayList();
	public static Map<Integer, Double> SUCCESS_CHANCES = Maps.newHashMap();
	public static Map<String, StoneItem> STONE_ITEMS = Maps.newHashMap();
	public static Map<String, AmuletItem> AMULET_ITEMS = Maps.newHashMap();
	public static Map<LoreStatsEnum, Double> UP_STATS = Maps.newHashMap();
	public static Map<LoreStatsEnum, Integer> STAT_LIMIT_LEVELS = Maps.newHashMap();	
	public static Map<Enchantment, Integer> UP_ENCHANTS = Maps.newHashMap();
	public static Map<Enchantment, Integer> ENCHANT_LIMIT_LEVELS = Maps.newHashMap();
	public static Map<String, String> STONE_FUSION_TOS = Maps.newHashMap();
	public static Map<String, Double> STONE_FUSION_CHANCES = Maps.newHashMap();
	
	public static void reload(FileConfiguration config) {
		TITLE_SHOW = new Double(20 * config.getDouble("title")).intValue();
		TITLE_FADE = new Double(20 * config.getDouble("title-fade")).intValue();
		
		LEVEL_LINE_FORMAT = config.getString("nangcap.name-suffix").replace("&", "§");
		
		ALLOWED_MATERIALS = config.getStringList("nangcap.materials");
		
		SUCCESS_CHANCES.clear();
		config.getStringList("nangcap.success-chance").forEach(s -> {
			SUCCESS_CHANCES.put(Integer.valueOf(s.split(":")[0]), Double.valueOf(s.split(":")[1]));
		});
		
		STONE_ITEMS.clear();
		config.getConfigurationSection("nangcap.stone").getKeys(false).forEach(id -> {
			ItemStack is = ItemBuilder.buildItem(config.getConfigurationSection("nangcap.stone." + id + ".item"));
			double multipleChance = config.getDouble("nangcap.stone." + id + ".multiple-chance");
			STONE_ITEMS.put(id, new StoneItem(id, multipleChance, is));
		});
		
		AMULET_ITEMS.clear();
		config.getConfigurationSection("nangcap.amulet").getKeys(false).forEach(id -> {
			ItemStack is = ItemBuilder.buildItem(config.getConfigurationSection("nangcap.amulet." + id + ".item"));
			double bonusChance = config.getDouble("nangcap.amulet." + id + ".bonus-chance");
			AMULET_ITEMS.put(id, new AmuletItem(bonusChance, is));
		});
		
		UP_STATS.clear();
		config.getConfigurationSection("nangcap.up-stat").getKeys(false).forEach(stat -> {
			UP_STATS.put(LoreStatsEnum.get(stat), config.getDouble("nangcap.up-stat." + stat));
		});
		
		STAT_LIMIT_LEVELS.clear();
		config.getConfigurationSection("nangcap.stat-limit-level").getKeys(false).forEach(stat -> {
			STAT_LIMIT_LEVELS.put(LoreStatsEnum.get(stat), config.getInt("nangcap.stat-limit-level." + stat));
		});
		
		UP_ENCHANTS.clear();
		config.getConfigurationSection("nangcap.up-enchant").getKeys(false).forEach(enchant -> {
			UP_ENCHANTS.put(Enchantment.getByName(enchant.toUpperCase()), config.getInt("nangcap.up-enchant." + enchant));
		});
		
		ENCHANT_LIMIT_LEVELS.clear();
		config.getConfigurationSection("nangcap.enchant-limit-level").getKeys(false).forEach(enchant -> {
			ENCHANT_LIMIT_LEVELS.put(Enchantment.getByName(enchant.toUpperCase()), config.getInt("nangcap.enchant-limit-level." + enchant));
		});
		
		STONE_FUSION_TOS.clear();
		config.getConfigurationSection("dunghop").getKeys(false).forEach(id -> {
			STONE_FUSION_TOS.put(id, config.getString("dunghop." + id + ".to"));
		});
		
		STONE_FUSION_CHANCES.clear();
		config.getConfigurationSection("dunghop").getKeys(false).forEach(id -> {
			STONE_FUSION_CHANCES.put(id, config.getDouble("dunghop." + id + ".chance"));
		});
	}
	
}
