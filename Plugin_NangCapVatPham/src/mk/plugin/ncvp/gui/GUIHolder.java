package mk.plugin.ncvp.gui;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class GUIHolder implements InventoryHolder {
	
	private String guiID;

	public GUIHolder(String guiID) {
		this.guiID = guiID;
	}
	
	public String getGUIID() {
		return this.guiID;
	}
	
	@Override
	public Inventory getInventory() {
		return null;
	}
	
}
