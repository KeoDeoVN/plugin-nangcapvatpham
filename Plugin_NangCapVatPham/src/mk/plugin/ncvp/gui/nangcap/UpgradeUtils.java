package mk.plugin.ncvp.gui.nangcap;

import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import api.praya.myitems.builder.lorestats.LoreStatsEnum;
import mk.plugin.ncvp.config.Configs;
import mk.plugin.ncvp.utils.ItemStackUtils;
import mk.plugin.ncvp.utils.MIUtils;
import mk.plugin.ncvp.utils.Utils;

public class UpgradeUtils {
	
	public static void setUpgradeable(ItemStack item) {
		if (!Configs.ALLOWED_MATERIALS.contains(item.getType().name())) return;
		if (canUpgrade(item)) return;
		ItemStackUtils.setDisplayName(item, ItemStackUtils.getName(item) + Configs.LEVEL_LINE_FORMAT.replace("%level%", "0"));
	}
	
	public static boolean canUpgrade(ItemStack item) {
		if (item == null) return false;
		if (!Configs.ALLOWED_MATERIALS.contains(item.getType().name())) return false;
		String line = ItemStackUtils.getName(item);
		String regex = Utils.getEscapeRegex(Configs.LEVEL_LINE_FORMAT).replace("%level%", "(?<level>" + Utils.getDoubleRegex() + ")");
		Pattern p = Pattern.compile(regex);
		Matcher matcher = p.matcher(line);
		while (matcher.find()) {
			return true;
		}
		return false;
	}
	
	public static int getLevelInLine(String line) {
		String regex = Utils.getEscapeRegex(Configs.LEVEL_LINE_FORMAT).replace("%level%", "(?<level>" + Utils.getDoubleRegex() + ")");
		Pattern p = Pattern.compile(regex);
		Matcher matcher = p.matcher(line);
		while (matcher.find()) {
			return Integer.valueOf(matcher.group("level"));
		}
		return 0;
	}
	
	public static String getLevelFormat(int level) {
		return Configs.LEVEL_LINE_FORMAT.replace("%level%", level + "");
	}
	
	public static int getLevel(ItemStack item) {
		return getLevelInLine(ItemStackUtils.getName(item));
	}
	
	public static void setLevelLine(ItemStack item, int oldlv, int lv) {
		String line = ItemStackUtils.getName(item);
		line = line.replace(getLevelFormat(oldlv), "");
		ItemStackUtils.setDisplayName(item, line + getLevelFormat(lv));
	}
	
	public static boolean checkLevelRequirement(ItemStack item) {
		int currentlv = getLevel(item);
		
		int maxlv = 15;
		for (Entry<Integer, Double> chances : Configs.SUCCESS_CHANCES.entrySet()) {
			if (chances.getValue() == 0) {
				maxlv = chances.getKey();
				break;
			}
		}
		
		return currentlv < maxlv;
	}
	
	public static void upgradeUp(ItemStack item, int nextlv) {
		int currentlv = getLevel(item);
		
		// Set stat
		Map<LoreStatsEnum, LoreStat> statBefore = MIUtils.getStats(item);
		Map<LoreStatsEnum, LoreStat> statAfter = Maps.newHashMap(statBefore);
		for (int i = currentlv + 1 ; i <= nextlv ; i++) {
			if (i == 0) break;
			int checkingLevel = i;
			statBefore.keySet().forEach(stat -> {
				// Check max level 
				if (Configs.STAT_LIMIT_LEVELS.containsKey(stat)) {
					if (Configs.STAT_LIMIT_LEVELS.get(stat) <= checkingLevel) return;
				}
				
				// Do
				double up = Configs.UP_STATS.get(stat);
				LoreStat ls = statAfter.get(stat);
				ls.setValue(ls.getValue() * (1 + up));
				ls.setMaxValue(ls.getMaxValue() * (1 + up));
			});
		}
		
		statAfter.forEach((stat, ls) -> {
			int index = MIUtils.getLoreStatManager().getLineLoreStats(item, stat) - 1;
			if (index == -2) return;
			String line = MIUtils.getStatLine(stat, ls);
			ItemStackUtils.setLoreLine(item, line, index);
		});
		
		// Set enchants
		Map<Enchantment, Integer> enchantBefore = item.getEnchantments();
		for (int i = currentlv + 1 ; i <= nextlv ; i++) {
			if (i == 0) break;
			
			int checkingLevel = i;
			enchantBefore.forEach((e, lv) -> {
				// Check max level 
				if (Configs.ENCHANT_LIMIT_LEVELS.containsKey(e)) {
					if (Configs.ENCHANT_LIMIT_LEVELS.get(e) <= checkingLevel) return;
				}
				
				// Up
				if (!Configs.UP_ENCHANTS.containsKey(e)) return;
				item.addUnsafeEnchantment(e, lv + Configs.UP_ENCHANTS.get(e));
			});
		}

		
		// Set NAME, LORE
		setLevelLine(item, currentlv, nextlv);
	}
	
	public static void upgradeDown(ItemStack item, int nextlv) {
		int currentlv = getLevel(item);
		
		Map<LoreStatsEnum, LoreStat> statBefore = MIUtils.getStats(item);
		Map<LoreStatsEnum, LoreStat> statAfter = Maps.newHashMap(statBefore);
		
		for (int i = currentlv ; i > nextlv ; i--) {
			if (i == 0) break;
			
			int checkingLevel = i;
			statBefore.keySet().forEach(stat -> {
				// Check max level 
				if (Configs.STAT_LIMIT_LEVELS.containsKey(stat)) {
					if (Configs.STAT_LIMIT_LEVELS.get(stat) <= checkingLevel) return;
				}
				
				double up = Configs.UP_STATS.get(stat);
				LoreStat ls = statAfter.get(stat);
				ls.setValue((double) ls.getValue() / (1 + up));
				ls.setMaxValue((double) ls.getMaxValue() / (1 + up));
			});
		}
		
		statAfter.forEach((stat, ls) -> {
			int index = MIUtils.getLoreStatManager().getLineLoreStats(item, stat) - 1;
			if (index == -2) return;
			String line = MIUtils.getStatLine(stat, ls);
			ItemStackUtils.setLoreLine(item, line, index);
		});
		
		
		// Set enchants
		Map<Enchantment, Integer> enchantBefore = item.getEnchantments();
		for (int i = currentlv ; i > nextlv ; i--) {
			if (i == 0) break;
			
			int checkingLevel = i;
			enchantBefore.forEach((e, lv) -> {
				// Check max level 
				if (Configs.ENCHANT_LIMIT_LEVELS.containsKey(e)) {
					if (Configs.ENCHANT_LIMIT_LEVELS.get(e)  <= checkingLevel) return;
				}
				
				// Up
				if (!Configs.UP_ENCHANTS.containsKey(e)) return;
				item.addUnsafeEnchantment(e, lv + Configs.UP_ENCHANTS.get(e));
			});
		}

		
		// Set NAME, LORE
		setLevelLine(item, currentlv, nextlv);
	}
	
	public static void upgrade(ItemStack item, int nextlv) {
		int current = getLevel(item);
		if (current < nextlv) upgradeUp(item, nextlv);
		else upgradeDown(item, nextlv);
	}
	
	
	
}
