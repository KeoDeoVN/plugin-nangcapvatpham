package mk.plugin.ncvp.gui.nangcap;

import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import mk.plugin.ncvp.config.Configs;
import mk.plugin.ncvp.gui.GUIHandler;
import mk.plugin.ncvp.gui.Icon;
import mk.plugin.ncvp.item.amulet.AmuletItem;
import mk.plugin.ncvp.item.stone.StoneItem;
import mk.plugin.ncvp.lang.Lang;
import mk.plugin.ncvp.utils.GUIUtils;
import mk.plugin.ncvp.utils.Utils;

public class UpgradeGUIHandler implements GUIHandler {

	@Override
	public void onTopClick(Inventory inv, Player player, int slot, ClickType clickType, InventoryClickEvent e) {
		Icon i = GUIUtils.fromInventory(inv).getIcon(slot);
		if (clickType == ClickType.SHIFT_LEFT || clickType == ClickType.SHIFT_LEFT) {
			player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
			if (i == null || !i.isCloseDrop()) return;
			ItemStack item = GUIUtils.getItem(inv, slot);
			if (item == null) return;
			inv.setItem(slot, GUIUtils.getDefaultIcon(inv, slot));
			Utils.giveItem(player, item);
			resetButton(inv, player);
			
			// Check if STONE
			if (i.getSlotType().equalsIgnoreCase("item")) {
				GUIUtils.setItem(inv, "result", GUIUtils.getDefaultIcon(inv, "result"));
			}
		}
	}

	@Override
	public void onBotClick(Inventory topinv, Inventory inv, Player player, int slot, ClickType clickType, InventoryClickEvent e) {
		ItemStack clickedItem = inv.getItem(slot);
		// Shift click
		if (clickType == ClickType.SHIFT_LEFT || clickType == ClickType.SHIFT_RIGHT) {
			player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
			e.setCancelled(true);

			// Stone
			if (StoneItem.isStoneItem(clickedItem)) {
				if (!GUIUtils.placeItem(topinv, clickedItem, "stone")) {
					player.sendMessage(Lang.STONE_DADAT.get());
					return;
				}
			}
			
			// Amulet
			else if (AmuletItem.isAmuletItem(clickedItem)) {
				if (!GUIUtils.placeItem(topinv, clickedItem, "amulet")) {
					player.sendMessage(Lang.BUAHOMENH_DADAT.get());
					return;
				}
			}

			// Item
			else if (UpgradeUtils.canUpgrade(clickedItem)) {
				int lv = UpgradeUtils.getLevel(clickedItem);
				
				// Check level
				if (!UpgradeUtils.checkLevelRequirement(clickedItem)) {
					player.sendMessage(Lang.VATPHAM_GIOIHAN.get());
					player.closeInventory();
					return;
				}
				
				// Place
				ItemStack result = clickedItem.clone();
				if (!GUIUtils.placeItem(topinv, clickedItem, "item")) {
					player.sendMessage(Lang.VATPHAM_DADAT.get());
					return;
				}
				
				// Set result
				UpgradeUtils.upgrade(result, lv + 1);
				GUIUtils.placeItem(topinv, result, "result");
			}
			
			else if (clickedItem != null && clickedItem.getType() != Material.AIR) {
				player.sendMessage(Lang.WRONG_ITEM.get());
				player.closeInventory();
				return;
			}
			
			// Reset button
			resetButton(topinv, player);
		}
	}

	@Override
	public void onButtonClick(Inventory inv, Player player, InventoryClickEvent event) {
		player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
		double chance = getSuccessChance(inv, player, true);
		if (chance == 0) return;
		
		ItemStack item = GUIUtils.getItem(inv, "item");
		int currentlv = UpgradeUtils.getLevel(item);
		int newlv = currentlv;

		if (Utils.rate(chance)) {
			newlv = currentlv + 1;
			player.sendTitle(Lang.CUONGHOA_THANHCONG_TITLE.get().replace("%oldlevel%", currentlv + "").replace("%newlevel%", newlv + ""), Lang.CUONGHOA_THANHCONG_SUBTITLE.get().replace("%oldlevel%", currentlv + "").replace("%newlevel%", newlv + ""), Configs.TITLE_FADE, Configs.TITLE_SHOW, Configs.TITLE_FADE);
			player.sendMessage(Lang.CUONGHOA_THANHCONG_MESSAGE.get().replace("%oldlevel%", currentlv + "").replace("%newlevel%", newlv + ""));
			player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);
		}
		else {
			player.sendTitle(Lang.CUONGHOA_THATBAI_TITLE.get().replace("%oldlevel%", currentlv + "").replace("%newlevel%", newlv + ""), Lang.CUONGHOA_THATBAI_SUBTITLE.get().replace("%oldlevel%", currentlv + "").replace("%newlevel%", newlv + ""), Configs.TITLE_FADE, Configs.TITLE_SHOW, Configs.TITLE_FADE);
			player.sendMessage(Lang.CUONGHOA_THATBAI_MESSAGE.get().replace("%oldlevel%", currentlv + "").replace("%newlevel%", newlv + ""));
			player.playSound(player.getLocation(), Sound.ENTITY_GHAST_SCREAM, 1, 1);
		}
		
		ItemStack result = item.clone();
		UpgradeUtils.upgrade(result, newlv);
		
		GUIUtils.setItem(inv, "item", null);
		GUIUtils.setItem(inv, "stone", null);
		GUIUtils.setItem(inv, "amulet", null);
		
		Utils.giveItem(player, result);
		
		player.closeInventory();
	}

	@Override
	public Map<String, String> getPlaceholders(Inventory inv, Player player) {
		Map<String, String> placeholders = Maps.newHashMap();
		placeholders.put("success_chance", 0d + "");
		placeholders.put("failure_chance", 100d + "");
		return placeholders;
	}

	@Override
	public double getSuccessChance(Inventory inv, Player player, boolean warn) {
		// Item
		ItemStack item = GUIUtils.getItem(inv, "item");
		if (item == null) {
			if (warn) player.sendMessage(Lang.VATPHAM_THIEU.get());
			return 0;
		}
		int nextlv = UpgradeUtils.getLevel(item) + 1;
		
		// Source 
		double chance = Configs.SUCCESS_CHANCES.get(nextlv);
		
		// Gem
		boolean has = false;
		for (ItemStack stone : GUIUtils.getItemList(inv, "stone")) {
			if (stone == null) continue;
			chance *= StoneItem.match(stone).getMultipleChance();
			has = true;
		}
		if (warn && !has) {
			player.sendMessage(Lang.STONE_THIEU.get());
			return 0;
		}
		
		// Lucky
		List<ItemStack> bhm = GUIUtils.getItemList(inv, "amulet");
		for (ItemStack is : bhm) {	
			chance += AmuletItem.match(is).getBonusChance();
		}
		
		return Math.min(chance, 100);
	}
	@Override
	public void resetButton(Inventory inv, Player player) {
		double chance = getSuccessChance(inv, player, false);
		Map<String, String> placeholders = getPlaceholders(inv, player);
		placeholders.put("success_chance", Utils.round(chance) + "");
		placeholders.put("failure_chance", Utils.round(100 - chance) + "");
		GUIUtils.resetItem(inv, "button", placeholders);
	}

}
