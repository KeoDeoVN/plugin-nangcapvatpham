package mk.plugin.ncvp.gui.nangcap;

public class LoreStat {
	
	private int line;
	private double minValue;
	private double maxValue;
	
	public LoreStat(int line, double minValue, double maxValue) {
		this.line = line;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	public int getLine() {
		return this.line;
	}
	
	public double getValue() {
		return this.minValue;
	}
	
	public double getMaxValue() {
		return this.maxValue;
	}
	
	public boolean hasMinMax() {
		return this.maxValue > this.minValue;
	}
	
	public void setValue(double value) {
		this.minValue = value;
	}
	
	public void setMaxValue(double value) {
		this.maxValue = value;
	}
}
