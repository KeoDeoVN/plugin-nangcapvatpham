package mk.plugin.ncvp.gui;

import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public interface GUIHandler {
	
	// Executors
	public void onTopClick(Inventory inv, Player player, int slot, ClickType clickType, InventoryClickEvent e);
	public void onBotClick(Inventory topinv, Inventory botinv, Player player, int slot, ClickType clickType, InventoryClickEvent e);
	public void onButtonClick(Inventory inv, Player player, InventoryClickEvent event);
	
	// Getters
	public Map<String, String> getPlaceholders(Inventory inv, Player player);
	public double getSuccessChance(Inventory inv, Player player, boolean warn);
	
	// Reset button
	public void resetButton(Inventory inv, Player player);
	
}
