package mk.plugin.ncvp.gui.dunghop;

import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import mk.plugin.ncvp.config.Configs;
import mk.plugin.ncvp.gui.GUIHandler;
import mk.plugin.ncvp.gui.Icon;
import mk.plugin.ncvp.item.amulet.AmuletItem;
import mk.plugin.ncvp.item.stone.StoneItem;
import mk.plugin.ncvp.lang.Lang;
import mk.plugin.ncvp.utils.GUIUtils;
import mk.plugin.ncvp.utils.Utils;

public class FusionGUIHandler implements GUIHandler {

	@Override
	public void onTopClick(Inventory inv, Player player, int slot, ClickType clickType, InventoryClickEvent e) {
		Icon i = GUIUtils.fromInventory(inv).getIcon(slot);
		if (clickType == ClickType.SHIFT_LEFT || clickType == ClickType.SHIFT_LEFT) {
			player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
			if (i == null || !i.isCloseDrop()) return;
			ItemStack item = GUIUtils.getItem(inv, slot);
			if (item == null) return;
			inv.setItem(slot, GUIUtils.getDefaultIcon(inv, slot));
			Utils.giveItem(player, item);
			resetButton(inv, player);
			
			// Check if STONE
			if (i.getSlotType().equalsIgnoreCase("stone")) {
				GUIUtils.setItem(inv, "result", GUIUtils.getDefaultIcon(inv, "result"));
			}
		}
		
	}

	@Override
	public void onBotClick(Inventory topinv, Inventory botinv, Player player, int slot, ClickType clickType, InventoryClickEvent e) {
		ItemStack clickedItem = botinv.getItem(slot);
		// Shift click
		if (clickType == ClickType.SHIFT_LEFT || clickType == ClickType.SHIFT_RIGHT) {
			player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
			e.setCancelled(true);		
			// Lucky
			if (AmuletItem.isAmuletItem(clickedItem)) {
				if (!GUIUtils.placeItem(topinv, clickedItem, "amulet")) {
					player.sendMessage(Lang.BUAHOMENH_DADAT.get());
					return;
				}
			}
			
			else if (StoneItem.isStoneItem(clickedItem)) {
				// Check
				StoneItem si = StoneItem.match(clickedItem);
				if (Configs.STONE_FUSION_TOS.get(si.getID()) == null) {
					player.sendMessage(Lang.DUNGHOP_KHONGTHE.get());
					return;
				}
				
				// Check equals previous
				List<ItemStack> stones = GUIUtils.getItemList(topinv, "stone");
				for (ItemStack is : stones) {
					if (!is.isSimilar(clickedItem)) {
						player.sendMessage(Lang.WRONG_ITEM.get());
						player.closeInventory();
						return;
					}
				}
				
				if (!GUIUtils.placeItem(topinv, clickedItem, "stone")) {
					player.sendMessage(Lang.STONE_DADAT.get());
					return;
				}
				
				ItemStack result = Configs.STONE_ITEMS.get(Configs.STONE_FUSION_TOS.get(si.getID())).getItemStack();
				GUIUtils.placeItem(topinv, result, "result");
			}
			
			else if (clickedItem != null && clickedItem.getType() != Material.AIR) {
				player.sendMessage(Lang.WRONG_ITEM.get());
				player.closeInventory();
				return;
			}
			
			// Reset button
			resetButton(topinv, player);
		}
	}

	@Override
	public void onButtonClick(Inventory inv, Player player, InventoryClickEvent event) {
		player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
		double chance = getSuccessChance(inv, player, true);
		if (chance == 0) return;
		
		if (!GUIUtils.fillAll(inv, "stone")) {
			player.sendMessage(Lang.STONE_THIEU.get());
			return;
		}
		ItemStack stone = GUIUtils.getItem(inv, "stone");
		StoneItem si = StoneItem.match(stone);
		
		ItemStack result = null;
		if (Utils.rate(chance)) {
			result = Configs.STONE_ITEMS.get(Configs.STONE_FUSION_TOS.get(si.getID())).getItemStack();
			player.sendTitle(Lang.DUNGHOP_THANHCONG_TITLE.get(), Lang.DUNGHOP_THANHCONG_SUBTITLE.get(), Configs.TITLE_FADE, Configs.TITLE_SHOW, Configs.TITLE_FADE);
			player.sendMessage(Lang.DUNGHOP_THANHCONG_MESSAGE.get());
			player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);
		}
		else {
			player.sendTitle(Lang.DUNGHOP_THATBAI_TITLE.get(), Lang.DUNGHOP_THATBAI_SUBTITLE.get(), Configs.TITLE_FADE, Configs.TITLE_SHOW, Configs.TITLE_FADE);
			player.sendMessage(Lang.DUNGHOP_THATBAI_MESSAGE.get());
			player.playSound(player.getLocation(), Sound.ENTITY_GHAST_SCREAM, 1, 1);
		}
		
		GUIUtils.setItem(inv, "stone", null);
		GUIUtils.setItem(inv, "amulet", null);
		
		if (result != null) Utils.giveItem(player, result);

		player.closeInventory();
	}

	@Override
	public Map<String, String> getPlaceholders(Inventory inv, Player player) {
		Map<String, String> placeholders = Maps.newHashMap();
		placeholders.put("success_chance", 0d + "");
		placeholders.put("failure_chance", 100d + "");
		return placeholders;
	}

	@Override
	public double getSuccessChance(Inventory inv, Player player, boolean warn) {
		// Socket
		ItemStack stone = GUIUtils.getItem(inv, "stone");
		if (stone == null) {
			if (warn) player.sendMessage(Lang.STONE_THIEU.get());
			return 0;
		}
		StoneItem si = StoneItem.match(stone);
		
		double chance = Configs.STONE_FUSION_CHANCES.get(si.getID());
		
		// Buahomenh
		List<ItemStack> bhm = GUIUtils.getItemList(inv, "amulet");
		for (ItemStack is : bhm) {	
			chance += AmuletItem.match(is).getBonusChance();
		}
		
		return Math.min(chance, 100);
	}

	@Override
	public void resetButton(Inventory inv, Player player) {
		double chance = getSuccessChance(inv, player, false);
		Map<String, String> placeholders = getPlaceholders(inv, player);
		placeholders.put("success_chance", Utils.round(chance) + "");
		placeholders.put("failure_chance", Utils.round(100 - chance) + "");
		GUIUtils.resetItem(inv, "button", placeholders);
	}

}
