package mk.plugin.ncvp.gui;

import java.util.List;
import java.util.Map;

import org.bukkit.inventory.ItemStack;

import mk.plugin.ncvp.utils.ItemStackUtils;
import mk.plugin.ncvp.utils.Utils;

public class Icon {
	
	private String slotType;
	private boolean isCloseDrop;
	private ItemStack defaultItem;
	
	public Icon(String slotType, boolean isCloseDrop, ItemStack defaultItem) {
		this.slotType = slotType;
		this.isCloseDrop = isCloseDrop;
		this.defaultItem = defaultItem;
	}

	public String getSlotType() {
		return this.slotType;
	}
	
	public boolean isCloseDrop() {
		return this.isCloseDrop;
	}
	
	public ItemStack getDefaultItem() {
		return this.defaultItem.clone();
	}
	
	public ItemStack getItem(Map<String, String> placeholders) {
		ItemStack icon = defaultItem.clone();
		if (placeholders != null) {
			ItemStackUtils.setDisplayName(icon, Utils.checkPlaceholders(ItemStackUtils.getName(icon), placeholders));
			List<String> lore = ItemStackUtils.getLore(icon);
			for (int i = 0 ; i < lore.size() ; i++) {
				lore.set(i, Utils.checkPlaceholders(lore.get(i), placeholders));
			}
			ItemStackUtils.setLore(icon, lore);
		}
		return icon;
	}
	
}
