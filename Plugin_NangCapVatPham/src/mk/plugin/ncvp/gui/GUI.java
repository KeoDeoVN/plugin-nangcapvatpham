package mk.plugin.ncvp.gui;

import java.util.Map;

public class GUI {
	
	private String title;
	private int row;
	private Map<Integer, Icon> icons;
	private GUIHandler handler;
	
	public GUI(String title, int row, Map<Integer, Icon> icons, GUIHandler handler) {
		this.title = title;
		this.row = row;
		this.icons = icons;
		this.handler = handler;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public int getRow() {
		return this.row;
	}
	
	public Map<Integer, Icon> getIcons() {
		return this.icons;
	}
	
	public Icon getIcon(int slot) {
		return this.icons.getOrDefault(slot, null);
	}
	
	public boolean hasIcon(int slot) {
		return icons.containsKey(slot);
	}
	
	public GUIHandler getHandler() {
		return this.handler;
	}
	
}
