package mk.plugin.ncvp.lang;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

import mk.plugin.ncvp.yaml.YamlFile;

public enum Lang {
	
	ONE_ITEM_ONLY("Chi duoc de 1 item"),
	
	NO_PERMISSION("Khong co quyen"),
	WRONG_ITEM("Sai item roi"),
	
	GIVE_ITEM("Nhan duoc %name% x%amount%"),
	GUI_KHONGTHEDAT("Để item xuống ô dưới và Shift + Click để đặt"),
	
	VATPHAM_THIEU("&cThiếu vật phẩm"),
	VATPHAM_DADAT("&cĐã để Vật phẩm rồi"),
	VATPHAM_GIOIHAN("&cVật phẩm đạt cấp độ giới hạn"),
	
	STONE_THIEU("Thieu stone"),
	STONE_DADAT("Da dat stone roi hihi"),
	
	BUAHOMENH_DADAT("Da dat bua ho menh roi"),
	
	CUONGHOA_WHITELIST("Khong the nang cap loai item nay"),
	CUONGHOA_KHONGTHE("Khong the nang cap"),
	CUONGHOA_THANHCONG_TITLE("Thanh cong"),
	CUONGHOA_THANHCONG_SUBTITLE("Thanh cong vl"),
	CUONGHOA_THANHCONG_MESSAGE("Thanh cong vl"),
	CUONGHOA_THATBAI_TITLE("That bai huhu"),
	CUONGHOA_THATBAI_SUBTITLE("Em den lam"),
	CUONGHOA_THATBAI_MESSAGE("Den vl"),
	
	DUNGHOP_KHONGTHE("Khong the dung hop"),
	DUNGHOP_THANHCONG_TITLE("Dung hop thanh cong"),
	DUNGHOP_THANHCONG_SUBTITLE("Hehe"),
	DUNGHOP_THANHCONG_MESSAGE("Duoc ban"),
	DUNGHOP_THATBAI_TITLE("That bai roi huhu"),
	DUNGHOP_THATBAI_SUBTITLE("Du me den vl"),
	DUNGHOP_THATBAI_MESSAGE("That bai :("),
	
	;

	
	private String value;
	
	private Lang(String value) {
		this.value = value;
	}
	
	public String get() {
		return this.value.replace("&", "§");
	}
	
	public void set(String value) {
		this.value = value;
	}
	
	public String getName() {
		return this.name().toLowerCase().replace("_", "-").replace("&", "§");
	}
	
	public void send(Player player, Map<String, String> placeholders) {
		String s = this.get();
		for (Entry<String, String> e : placeholders.entrySet()) {
			s = s.replace(e.getKey(), e.getValue());
		}
		player.sendMessage(s);
	}
	
	public void send(Player player) {
		String s = this.get();
		player.sendMessage(s);
	}
	
	public void broadcast(String key, String value) {
		Map<String, String> m = Maps.newHashMap();
		m.put(key, value);
		Bukkit.getOnlinePlayers().forEach(p -> {
			this.send(p, m);
		});
	}
	
	public void send(Player player, String key, String value) {
		Map<String, String> placeholders = Maps.newHashMap();
		placeholders.put(key, value);
		send(player, placeholders);
	}
	
	public static void reload(JavaPlugin plugin, YamlFile lang) {
		FileConfiguration config = lang.get();
		for (Lang l : Lang.values()) {
			if (config.contains(l.getName())) {
				l.set(config.getString(l.getName()).replace("&", "§"));
			}
			else {
				config.set(l.getName(), l.get().replace("§", "&"));
				lang.save(plugin);
			}
		}
	}
	
}
