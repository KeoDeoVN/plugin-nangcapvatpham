package mk.plugin.ncvp.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import mk.plugin.ncvp.command.AdminCommand;
import mk.plugin.ncvp.command.PlayerCommand;
import mk.plugin.ncvp.config.Configs;
import mk.plugin.ncvp.lang.Lang;
import mk.plugin.ncvp.listener.GUIListener;
import mk.plugin.ncvp.utils.GUIUtils;
import mk.plugin.ncvp.yaml.YamlFile;

public class MainNCVP extends JavaPlugin {
	
	@Override
	public void onEnable() {
		this.reloadConfigs();
		this.registerListeners();
		this.registerCommands();
	}
	
	@Override
	public void onDisable() {
		Bukkit.getOnlinePlayers().forEach(player -> player.closeInventory());
	}
	
	public void reloadConfigs() {
		this.saveDefaultConfig();
		YamlFile.reloadAll(this);
		Lang.reload(this, YamlFile.LANG);
		Configs.reload(YamlFile.CONFIG.get());
		GUIUtils.loadAll();
	}
	
	public void registerListeners() {
		Bukkit.getPluginManager().registerEvents(new GUIListener(), this);
	}
	
	public void registerCommands() {
		this.getCommand("nangcapadmin").setExecutor(new AdminCommand());
		this.getCommand("cuonghoa").setExecutor(new PlayerCommand());
		this.getCommand("dunghop").setExecutor(new PlayerCommand());
	}
	
	public static MainNCVP get() {
		return MainNCVP.getPlugin(MainNCVP.class);
	}
	
}
